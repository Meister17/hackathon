# -*- coding: utf-8 -*-
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import datetime
import json
import pymongo, urllib2
from time import  time
import sys,re

from pymongo import MongoClient
#connection = MongoClient()

uri = 'mongodb://chetvil:twimomusic@ds045948.mongolab.com:45948/twimomusic'
connection = MongoClient(uri)
#db = connection.get_default_database()
#print db.name
db = connection.twimomusic

# Go to http://dev.twitter.com and create an app.
# The consumer key and secret will be generated for you after
consumer_key="1AkDYnRmOULPhWvlEmXVJQ"
consumer_secret="b6Klbhs5FI9rjvCqZgR9M8hIJkRPsqi9ilINCRU4"

# After the step above, you will be redirected to your app's page.
# Create an access token under the the "Your access token" section
access_token="189833993-x27yaamF1joHdxaIGPYKLBw89SPwPLZ1UGcEgpit"
access_token_secret="vHcpotORRnaFLoatIk93ApOvUcQuuBZTtAzOIkYBxw"

sad = set()
with open("sad.txt","r") as f:
    data = f.read()
    sad = set(data.strip().split(", "))

happy = set()
with open("happy.txt","r") as f:
    data = f.read()
    happy = set(data.strip().split(", "))

angry = set()
with open("angry.txt","r") as f:
    data = f.read()
    angry = set(data.strip().split(", "))

def db_initpop(bundle):
    """
    This function places basic tweet features in the database.  Note the placeholder values:
    these can act as a check to verify that no further expansion was available for that method.
    """
    #unpack the bundle 
    tweet_id, user_id, user_sn, user_name, retweet_count, favorite_count, tweet_text, date, lng, lat, mood = bundle
    post = {"id": tweet_id,
         "user_id": user_id,
         "user": user_sn,
         "user_name": user_name,
         "retweet_count": retweet_count,
         "retweet_count": favorite_count,
         "text" : tweet_text,
         "date": datetime.datetime.utcnow(),
         "lng": lng,
         "lat": lat,
         "is_processed": False,
         "mood":mood}
    mood_posts = db.mood_posts
    post_id = mood_posts.insert(post)
    #print post_id

cnt = 0
start = time()

class StdOutListener(StreamListener):
    """ A listener handles tweets are the received from the stream.
    This is a basic listener that just prints received tweets to stdout.

    """
    def on_data(self, data):
        global cnt
        cnt += 1
        #print data
        tweet = json.loads(data)

        
        if 'delete' in tweet:
            delete = tweet['delete']['status']
            if self.on_delete(delete['id'], delete['user_id']) is False:
                return False
        elif 'limit' in tweet:
            if self.on_limit(tweet['limit']['track']) is False:
                return False
        elif 'warning' in tweet:
            warning = tweet['warnings']
            print warning['message']
            return False
        else:
            artist = ""
            track = ""

            if 'text' in tweet:
                text = tweet['text'].replace("&amp;","&")
            else:
                return True
            
            mood = ""
            tweet_words = text.lower().split()
            
            if "not" not in set(tweet_words) and "don't" not in set(tweet_words):
                for w in tweet_words:
                    if w in happy:
                        mood = "happy"
                        break
                    elif w in sad:
                        mood = "sad"
                        break
                    elif w in angry:
                        mood = "angry"
                        break
                    
            if True:#tweet['geo'] != None:
                for key in tweet:
                    
                    #if key == "geo":
                    #    print "coordinate lng", tweet[key]["coordinates"][0]
                    #    print "coordinate lat", tweet[key]["coordinates"][1]
                    #    print key, tweet[key]
                    #else:
                    #if key == 'text'or key == 'entities':
                    #print key, tweet[key]
                    if key == "user":
                        loc = tweet[key]['location']
                        #print loc
                        #print tweet[key]['location']
                        url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' + urllib2.quote(loc.encode('utf8')) + '&sensor=false'
                        #print url
                        try:
                            data = urllib2.urlopen(url).read()
                            doc = json.loads(data)
                        except:
                            print "Google failed to respond :("
                            pass
                        lat = 0
                        lng = 0
                        try:
                            for x in doc["results"]:
                                if type(x) == dict and "geometry" in x:
                                    lat = x["geometry"]["location"]["lat"]
                                    lng =  x["geometry"]["location"]["lng"]
                                    break
                        except:
                            lat = 0
                            lng = 0


                bundle = (tweet['id'], tweet['user']['id'],tweet['user']['screen_name'],
                          tweet['user']['name'], tweet['retweet_count'], tweet['favorite_count'], tweet['text'], tweet['created_at'], lat, lng, mood)
                if lat != 0 and lng != 0 and mood != "":
                    db_initpop(bundle)
                    #print tweet['text']
                    #print lat,lng
                    #print mood
                #print "\n"

        if cnt % 1000 == 0:
            print "rate %.1f tweets/sec)" % (cnt/(time()-start))
        return True
        
    def on_delete(self, status_id, user_id):
        sys.stdout.write( "Delete:"+ str(status_id) + "\n")
        return 

    def on_limit(self, track):
        sys.stdout.write("Limit:"+str(track) + "\n")
        return 
    
    def on_error(self, status):
        print 'Error at '+strftime("%d %b %Y %H:%M:%S", localtime())
        print status


#words = ["и","в","на","с","не","его","что","но","он","из","по","о","за","а","к","как"
#         ,"для","все","от","ее","их","у","они","чтобы","это","она","когда","только"]

words = list(sad)+list(angry)+list(happy)

l = StdOutListener()
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

stream = Stream(auth, l)
stream.filter(track=words)
