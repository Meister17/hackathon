#!/usr/bin/python
# -*- coding: utf-8 -*-
import tweepy.streaming
import tweepy
import simplejson
import datetime
import pymongo
import optparse
import sys
import re
import urllib2


class StdOutListener(tweepy.streaming.StreamListener):
    def __init__(self, url, regex, db):
        self.google_maps_url = url
        self.regex_list = regex
        self.database = db

    def on_data(self, data):
        tweet = simplejson.loads(data)
        if 'delete' in tweet or 'limit' in tweet or 'warning' in tweet:
            return False
        else:
            artist = ""
            track = ""
            if 'text' not in tweet:
                return True
            text = tweet['text'].replace("&amp;","&")
            for reg in self.regex_list:
                    find_res = reg.findall(text)
                    if len(find_res) > 0:
                        track = find_res[0][0].strip()
                        artist = find_res[0][1].strip()
                        break
            for key in tweet:
                if key == "user":
                    loc = tweet[key]['location']
                    url = (self.google_maps_url + 'json?address=' +
                           urllib2.quote(loc.encode('utf8')) + '&sensor=false')
                    data = urllib2.urlopen(url).read()
                    doc = simplejson.loads(data)
                    lat = 0
                    lng = 0
                    try:
                        for x in doc["results"]:
                            if type(x) == dict and "geometry" in x:
                                lat = x["geometry"]["location"]["lat"]
                                lng =  x["geometry"]["location"]["lng"]
                                break
                    except:
                        lat = 0
                        lng = 0
                elif key == "entities":
                    if (len(tweet["entities"]["user_mentions"]) > 0 and
                        tweet["entities"]["user_mentions"][0]["name"] != "musixmatch"):
                        artist = tweet["entities"]["user_mentions"][0]["name"]

            print "track=%s; artist=%s" % (track.encode('utf-8'), artist.encode('utf-8'))
            bundle = (tweet['id'], lat, lng, artist, track)
            self.database_save(bundle)
            return True

    def database_save(self, bundle):
        tweet_id, lng, lat, artist, title = bundle
        post = {"id": tweet_id, "timestamp": datetime.datetime.utcnow(), "lng": lng,
                "lat": lat, "processed": 0, "artist":artist, "title":title}
        self.database.posts.insert(post)


class TweetCatcher(object):
    def __init__(self, filename):
        with open(filename, 'r') as input:
            doc = simplejson.loads(input.read())
            self.mongo_url = doc['mongo_url']
            self.main_database = doc['main_database']
            self.consumer_key = doc['consumer_key']
            self.consumer_secret = doc['consumer_secret']
            self.access_token = doc['access_token']
            self.access_token_secret = doc['access_token_secret']
            self.echonest_error = float(doc['echonest_error'])
            self.regex_list = []
            with open(doc['regex_file'], 'r') as regex_file:
                for line in regex_file:
                    self.regex_list.append(re.compile(line.strip()))
            self.key_words = doc['key_words']
            self.google_maps_url = doc['google_maps_url']

    def catch_tweets(self):
        connection = pymongo.MongoClient(self.mongo_url)
        database = connection[self.main_database]
        listener = StdOutListener(self.google_maps_url, self.regex_list, database)
        oauth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
        oauth.set_access_token(self.access_token, self.access_token_secret)
        stream = tweepy.Stream(oauth, listener)
        stream.filter(track=self.key_words)


if __name__ == '__main__':
    parser = optparse.OptionParser(usage='Usage; %prog <config_file>')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Incorrect usage')

    config_filename = args[0]
    tweet_catcher = TweetCatcher(config_filename)
    tweet_catcher.catch_tweets()
