#!/usr/bin/python
import httplib
import simplejson
import optparse
import pymongo
import time
import urllib
import urllib2
import sys

from xml.sax.saxutils import unescape

def parse_config_file(filename):
    mongo_url = ''
    mongo_database = ''
    with open(filename, 'r') as input:
        doc = simplejson.loads(input.read())
        mongo_url = doc['mongo_url']
        mongo_database = doc['mongo_database']
    return mongo_url, mongo_database


def aggregate_music_tweets(url, database):
    connection = pymongo.MongoClient(url)
    database = connection[database]
    emotions_sum = {}
    for post in database.mood_posts.find({'is_processed': False}):
        if (post['lng'], post['lat']) not in emotions_sum:
            emotions_sum[(post['lng'], post['lat'])] = {'happy': 0.0, 'angry': 0.0, 'sad': 0.0}
        emotions_sum[(post['lng'], post['lat'])][post['mood']] += 1
    database.tweet_moods.remove()
    tweet_moods = database.tweet_moods
    for lng, lat in emotions_sum.keys():
        happy = emotions_sum[(lng, lat)]['happy']
        angry = emotions_sum[(lng, lat)]['angry']
        sad = emotions_sum[(lng, lat)]['sad']
        post = {'lng': lng, 'lat': lat}
        if happy > angry and happy > sad:
            post['mood'] = 'happy'
        elif angry > happy and angry > sad:
            post['mood'] = 'angry'
        elif sad > happy and sad > angry:
            post['mood'] = 'sad'
        elif happy > 0:
            post['mood'] = 'happy'
        else:
            post['mood'] = 'sad'
        post['radius'] = happy + angry + sad
        tweet_moods.insert(post)
    connection.disconnect()


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options]')
    parser.add_option('-c', '--config', dest='config', type=str,
                      default='api.conf', help='Configuration file')
    parser.add_option('-d', '--delay', dest='delay', type=int, default=10,
                      help='Delay before updating')
    options, args = parser.parse_args()
    if len(args) > 0:
        parser.error('Incorrect usage')

    mongo_url, mongo_database = parse_config_file(options.config)
    aggregate_music_tweets(mongo_url, mongo_database)