#!/usr/bin/python
import httplib
import simplejson
import optparse
import pymongo
import time
import urllib
import urllib2
import sys

from xml.sax.saxutils import unescape


class EchoNestParser(object):
    def __init__(self, filename):
        with open(filename, 'r') as input:
            doc = simplejson.loads(input.read())
            self.mongo_url = doc['mongo_url']
            self.main_database = doc['main_database']
            self.api_keys = doc['api_keys']
            self.echonest_url = doc['echonest_url']
            self.echonest_error = float(doc['echonest_error'])

    def process_tracks(self):
        tracks = {}
        connection = pymongo.MongoClient(self.mongo_url)
        database = connection[self.main_database]
        for index, post in enumerate(database.posts.find({"processed": 0})):
            if self.has_field(post, 'artist') and self.has_field(post, 'title'):
                energy = self.get_track_energy(
                    str(self.api_keys[index % len(self.api_keys)]),
                    urllib.quote(unescape(post['artist']).lower().encode('utf-8')),
                    urllib.quote(unescape(post['title']).lower().encode('utf-8')))
                if energy > self.echonest_error:
                    print 'Energy %g for %s ; %s' % (energy, post['artist'].encode('utf-8'),
                                                     post['title'].encode('utf-8'))
                    database.posts.update({'_id': post['_id']},
                                          {'$set': {'processed': 1,
                                                    'energy': energy}})
                else:
                    database.posts.update({'_id': post['_id']}, {'$set': {'processed': -1}})
                    print 'Failed to retrieve track: %s ; %s' % (post['artist'].encode('utf-8'),
                                                                 post['title'].encode('utf-8'))
        connection.disconnect()

    def has_field(self, post, field):
        return field in post and len(post[field]) > 0

    def get_track_energy(self, api_key, artist, title):
        try:
            url = (self.echonest_url + 'song/search?api_key=' + api_key +
                '&format=json&results=1&artist=' + artist + '&title=' + title +
                '&bucket=id:7digital-US&bucket=audio_summary&bucket=tracks')
            response = urllib2.urlopen(url).read()
            data = simplejson.loads(response)
            data = data['response']['songs'][0]['audio_summary']
            return float(data['energy'])
        except Exception:
            return self.echonest_error


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <config_file>')
    parser.add_option('-d', '--delay', dest='delay', type=int, default=10,
                      help='Delay before updating')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Incorrect usage')

    config_file = args[0]
    echonest_parser = EchoNestParser(config_file)
    while True:
        echonest_parser.process_tracks()
        time.sleep(options.delay)