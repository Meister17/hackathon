#!/usr/bin/python
import httplib
import simplejson
import optparse
import pymongo
import time
import urllib
import urllib2
import sys

from xml.sax.saxutils import unescape

class MoodsAggregator(object):
    def __init__(self, filename):
        with open(filename, 'r') as input:
            doc = simplejson.loads(input.read())
            self.mongo_url = doc['mongo_url']
            self.main_database = doc['main_database']
            self.moods_database = doc['moods_database']
            self.echonest_error = doc['echonest_error']

    def aggregate_music_emotions(self):
        connection = pymongo.MongoClient(self.mongo_url)
        database = connection[self.main_database]
        emotions_sum = {}
        emotions_number = {}
        for post in database.posts.find({'processed': 1}):
            if (post['lng'], post['lat']) not in emotions_number:
                emotions_sum[(post['lng'], post['lat'])] = self.get_empty_emotions()
                emotions_number[(post['lng'], post['lat'])] = self.get_empty_emotions()
            self.update_emotion(post, 'acousticness', emotions_sum, emotions_number)
            self.update_emotion(post, 'danceability', emotions_sum, emotions_number)
            self.update_emotion(post, 'energy', emotions_sum, emotions_number)
            self.update_emotion(post, 'loudness', emotions_sum, emotions_number)
            self.update_emotion(post, 'tempo', emotions_sum, emotions_number)
            database.posts.update({'id': post['id']}, {'$set': {'processed': 2}})

        database = connection[self.moods_database]
        for post in database.posts:
            if (post['lng'], post['lat']) in emotions_number:
                if self.is_good_record(emotions_number[(post['lng'], post['lat'])]):
                    new_post = self.update_post(post,
                                                emotions_sum[(post['lng'], post['lat'])],
                                                emotions_number[(post['lng'], post['lat'])])
                    database.posts.update({'id': post['id']}, {'$set': new_post})
                del(emotions_number[(post['lng'], post['lat'])])
                del(emotions_sum[(post['lng'], post['lat'])])

        for lng, lat in emotions_number.keys():
            if self.is_good_record(emotions_number[(lng, lat)]):
                post = {'lng': lng, 'lat': lat, 'radius': 0}
                post.update(self.get_empty_emotions())
                new_post = self.update_post(post, emotions_sum[(lng, lat)],
                                            emotions_number[(lng, lat)])
                database.insert(post)
        connection.disconnect()

    def get_empty_emotions(self):
        return {'acousticness': 0.0, 'danceability': 0.0, 'energy': 0.0,
                'loudness': 0.0, 'tempo': 0.0}

    def update_emotion(self, post, emotion, emotions_sum, emotions_number):
        if float(post[emotion]) > self.echonest_error:
            emotions_sum[(post['lng'], post['lat'])][emotion] += float(post[emotion])
            emotions_number[(post['lng'], post['lat'])][emotion] += 1

    def update_post(self, post, emotions_sum, emotions_number):
        new_post = {'lng': post['lng'], 'lat': post['lat']}
        new_post['acousticness'] = self.get_new_emotion_value(post, emotions_sum,
                                                              emotions_number, 'acousticness')
        new_post['danceability'] = self.get_new_emotion_value(post, emotions_sum,
                                                              emotions_number, 'danceability')
        new_post['energy'] = self.get_new_emotion_value(post, emotions_sum,
                                                        emotions_number, 'energy')
        new_post['loudness'] = self.get_new_emotion_value(post, emotions_sum,
                                                          emotions_number, 'loudness')
        new_post['tempo'] = self.get_new_emotion_value(post, emotions_sum,
                                                       emotions_number, 'tempo')
        new_post['radius'] = (int(post['radius']) +
            self.get_new_radius(emotions_number[(post['lng'], post['lat'])]))
        return new_post

    def get_new_emotion_value(self, post, emotions_sum, emotions_number, emotion):
        emotion_sum = emotions_sum[(post['lng'], post['lat'])][emotion]
        emotion_number = emotion_number[(post['lng'], post['lat'])][emotion_number]
        return (post[emotion] + emotion_sum) / (post['radius'] + emotion_number)

    def is_good_record(self, post):
        return (post['acousticness'] > 0 and post['danceability'] > 0 and
                post['energy'] > 0 and post['loudness'] > 0 and post['tempo'] > 0)

    def get_new_radius(self, post):
        return max(post['acousticness'], post['danceability'], post['energy'],
                   post['loudness'], post['tempo'])


if __name__ == '__main__':
    parser = optparse.OptionParser(
        usage='Usage: %prog [options] <config_file>')
    parser.add_option('-d', '--delay', dest='delay', type=int, default=1000,
                      help='Delay before updating')
    options, args = parser.parse_args()
    if len(args) != 1:
        parser.error('Incorrect usage')

    config_file = args[0]
    moods_aggregator = MoodsAggregator(config_file)
    while True:
        moods_aggregator.aggregate_music_emotions()
        time.sleep(options.delay)
